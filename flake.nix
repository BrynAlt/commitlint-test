{
  description = "Elm Environment";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:nixos/nixpkgs";
  };

  outputs = {
    flake-utils,
    nixpkgs,
    self,
    ...
  }:
    flake-utils.lib.eachDefaultSystem (
      system: let
        pkgs = nixpkgs.legacyPackages.${system};
      in {
        devShell = pkgs.mkShell {
          packages = with pkgs; [
            commitlint
            nodePackages."@commitlint/config-conventional"
            nodePackages.npm
          ];

          inputsFrom = with pkgs; [
            nodePackages."@commitlint/config-conventional"
          ];

          shellHook = ''
            git config core.hooksPath ./.hooks
          '';
        };
      }
    );
}
